---
title: JavaScript const
date: 2022-10-16 20:17:41
tags:
---

To declare variables that are constant, the '**const**' keyword is used. These constant variables are block scoped like the variables declared using let but they cannot be reassigned or redeclared. If the constant is an object or an array, its items can be updated or removed. This declaration creates a constant whose scope can be either global or local to the block in which it was declared, but global constants does not become the property of the window object unlike var variables. Also, they need to be initialised with a value during declaration as they can't be changed later.

## Mutating Constant Variables

Attempting to reassign a constant variable throws a TypeError: "Assignment to constant variable".

1. Behaviour of const on **String**, **Number** and **Boolean**:

```[javascript]
const demoNumber=1;
const demoString="Hello";
const demoBoolean=true;

demoNumber=7;
demoString="World";
demoBoolean=false;

// throws TypeError in all the three cases
```

Here, we cannot reassign any value(even the same value) in these variables.

2. Behaviour of const on **Arrays**:

Arrays also throw the same error when we try to reassign it.

```[javascript]
const demoArray = [1, 2, 3];
demoArray = [4,5,6];

console.log(demoArray); // throws TypeError
```

However, constant arrays can be mutated i.e its elements can be changed, or elements can be added or removed from a constant array. Functions can also be carried out in constant arrays.

```[javascript]
const demoArray = [1, 2, 3];

demoArray[0]=4;
console.log(demoArray); // demoArray=[4,2,3]

demoArray.push(5);
console.log(demoArray); // demoArray=[4,2,3,5]

demoArray.shift();
console.log(demoArray); // demoArray=[2,3,5]

demoArray.sort((currentItem,nextItem)=>{
    return nextItem-currentItem;
})
console.log(demoArray); // demoArray=[5,3,2]
```

3. Behaviour of const on **Objects**:

Objects also throw TypeError when we try to reassign it.

```[javascript]
const demoObject = { key1 : 1, key2 : 2 };
demoObject = { key3 : 3, key4 : 4, key5 : 5 };

console.log(demoObject); // throws TypeError
```

Similar to arrays, constant objects can also be mutated i.e properties can be changed, added or deleted.

```[javascript]
const demoObject = { key1 : 1, key2 : 2 };

demoObject.key3=3;
console.log(demoObject); // demoObject={ key1 : 1, key2 : 2, key3 : 3 }

demoObject.key2=4;
console.log(demoObject); // demoObject={ key1 : 1, key2 : 4, key3 : 3 }

delete demoObject.key1;
console.log(demoObject); // demoObject={ key2 : 4, key3 : 3 }
```

- So, in short, variables declared using 'const' cannot be reassigned or redeclared, but constant arrays and objects can be mutated but not reassigned.
